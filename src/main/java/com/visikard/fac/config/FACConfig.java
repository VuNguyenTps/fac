package com.visikard.fac.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Configuration
@Getter
public class FACConfig {
    @Value("${fac.base-url}")
    private String baseUrl;
    
    @Value("${fac.base-url}/PGServiceXML/Authorize3DS")
    private String authorize3DSUrl;
    
    @Value("${fac.base-url}/PGServiceXML/Authorize")
    private String authorizeUrl;
    
    @Value("${fac.base-url}/PGServiceXML/Tokenize")
    private String tokenizeUrl;
    
    @Value("${fac.merchant.id}")
    private String merchantId;
    
    @Value("${fac.merchant.password}")
    private String merchantPassword;
    
    @Value("${fac.acquirer.id}")
    private String acquirerId;
    
    @Value("${kount.server-url}")
    private String kountServerUrl;
    
    @Value("${kount.merchant-id}")
    private String kountMerchantId;
}

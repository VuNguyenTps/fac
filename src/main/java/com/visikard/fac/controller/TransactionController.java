package com.visikard.fac.controller;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.visikard.fac.config.FACConfig;

@Controller
public class TransactionController {
    @Autowired
    private FACConfig config;
    
    @GetMapping("/transaction")
    public String index(Model model) {
        model.addAttribute("KountServerUrl", config.getKountServerUrl());
        model.addAttribute("KountMerchantId", config.getKountMerchantId());
        model.addAttribute("sessionId", RandomStringUtils.randomAlphanumeric(32).toLowerCase());
        model.addAttribute("orderNumber", RandomStringUtils.randomAlphanumeric(8));
        model.addAttribute("amount", 0.2d);
        
        return "transaction";
    }
}

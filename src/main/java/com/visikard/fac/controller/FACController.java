package com.visikard.fac.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visikard.fac.dto.TransactionDto;
import com.visikard.fac.service.FACService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class FACController {

    @Autowired
    private FACService service;

    private ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping("/health")
    public String health() {
        return "alright";
    }

    @PostMapping("/secure3D")
    public String secure3D(@RequestBody TransactionDto transaction) throws JsonProcessingException {
        return service.authorize3DS(transaction);
    }

    @PostMapping("/capture")
    public String capture(@RequestBody TransactionDto transaction) throws JsonProcessingException {
        log.info(objectMapper.writeValueAsString(transaction));
        return service.capture(transaction);
    }
}

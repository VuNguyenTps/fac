package com.visikard.fac.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.visikard.fac.dto.TransactionDto;

public interface FACService {

    String authorize3DS(TransactionDto transaction) throws JsonProcessingException;

    String capture(TransactionDto transaction) throws JsonProcessingException;

}

package com.visikard.fac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.visikard.fac.config.FACConfig;
import com.visikard.fac.dto.Authorize3DSRequest;
import com.visikard.fac.dto.AuthorizeRequest;
import com.visikard.fac.dto.CardDetails;
import com.visikard.fac.dto.FraudDetails;
import com.visikard.fac.dto.TokenizeRequest;
import com.visikard.fac.dto.TokenizeResponse;
import com.visikard.fac.dto.TransactionDetails;
import com.visikard.fac.dto.TransactionDto;
import com.visikard.fac.service.FACService;

@Service
public class FACServiceImpl implements FACService {

    @Autowired
    private FACConfig config;

    private RestTemplate restTemplate = new RestTemplate();

    private TransactionDetails getTransactionDetails(TransactionDto transaction) {
        String merchantId = config.getMerchantId();
        String merchantPassword = config.getMerchantPassword();
        String acquirerId = config.getAcquirerId();

        TransactionDetails transactionDetails = new TransactionDetails(transaction);

        transactionDetails.setSignature(transactionDetails.generateSignature(merchantId, merchantPassword, acquirerId));
        transactionDetails.setCustomerReference("This is test for merchant " + merchantId);
        return transactionDetails;
    }

    private CardDetails getCardDetails() {
        return CardDetails.builder().cardCVV2("123").cardExpiryDate("0322").cardNumber("4111111111111111").customerName("Nguyen Vu").installments(0)
                .build();
    }

    @Override
    public String authorize3DS(TransactionDto transaction) throws JsonProcessingException {
        CardDetails cardDetails = getCardDetails();
        TransactionDetails transactionDetails = getTransactionDetails(transaction);

        Authorize3DSRequest requestBody = Authorize3DSRequest.builder().cardDetails(cardDetails).transactionDetails(transactionDetails)
                .merchantResponseURL("https://webhook.site/ca509c25-2c13-4907-baf6-543ab7242d92").build();
        String requestBodyXml = requestBody.toXML();
        System.out.println(requestBodyXml);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<String> request = new HttpEntity(requestBodyXml, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(config.getAuthorize3DSUrl(), request, String.class);

        return response.getBody();
    }

    @Override
    public String capture(TransactionDto transaction) throws JsonProcessingException {
        CardDetails cardDetails = getCardDetails();

        TokenizeRequest tokenizeRequest = new TokenizeRequest(cardDetails, config.getMerchantId(), config.getMerchantPassword(),
                config.getAcquirerId());
        String tokenizeBodyXml = tokenizeRequest.toXML();
        System.out.println(tokenizeBodyXml);

        HttpHeaders tokenizeHeaders = new HttpHeaders();
        tokenizeHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<String> tokenizeHttpRequest = new HttpEntity(tokenizeBodyXml, tokenizeHeaders);
        ResponseEntity<String> tokenizeHttpResponse = restTemplate.postForEntity(config.getTokenizeUrl(), tokenizeHttpRequest, String.class);

        String tokenizeResponseBody = tokenizeHttpResponse.getBody();
        TokenizeResponse tokenizeResponse = TokenizeResponse.fromXML(tokenizeResponseBody);

        cardDetails.setCardNumber(tokenizeResponse.getToken());

        TransactionDetails transactionDetails = getTransactionDetails(transaction);
        transactionDetails.setTransactionCode(9);

        FraudDetails fraudDetails = FraudDetails.builder().sessionId(transaction.getSessionId()).build();
        
        AuthorizeRequest requestBody = AuthorizeRequest.builder().cardDetails(cardDetails).transactionDetails(transactionDetails).fraudDetails(fraudDetails).build();
        String requestBodyXml = requestBody.toXML();
        System.out.println(requestBodyXml);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<String> request = new HttpEntity(requestBodyXml, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(config.getAuthorizeUrl(), request, String.class);

        return response.getBody();
    }

}

package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JacksonXmlRootElement(localName = "TokenizeResponse")
public class TokenizeResponse {
    @JsonProperty("ErrorMsg")
    private String errorMsg;
    
    @JsonProperty("Success")
    private Boolean success;
    
    @JsonProperty("Token")
    private String token;
    
    public static TokenizeResponse fromXML(String xmlString) throws JsonMappingException, JsonProcessingException {
        XmlMapper xmlMapper = new XmlMapper();
        return xmlMapper.readValue(xmlString, TokenizeResponse.class);
    }
}

package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RecurringDetails {
    @JsonProperty("ExecutionDate")
    private String executionDate = "";
    
    @JsonProperty("Frequency")
    private String frequency = "";
    
    @JsonProperty("IsRecurring")
    private Boolean isRecurring = false;
    
    @JsonProperty("NumberOfRecurrences")
    private Integer numberOfRecurrences = 0;
}

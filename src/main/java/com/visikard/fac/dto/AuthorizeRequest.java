package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@JacksonXmlRootElement(localName = "AuthorizeRequest", namespace = "http://schemas.firstatlanticcommerce.com/gateway/data")
public class AuthorizeRequest extends FACRequest {

    @JsonProperty("BillingDetails")
    private BillingDetails billingDetails;

}

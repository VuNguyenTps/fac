package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JacksonXmlRootElement(localName = "TokenizeRequest", namespace = "http://schemas.firstatlanticcommerce.com/gateway/data")
public class TokenizeRequest {
    @JsonProperty("CardNumber")
    private String cardNumber = "";
    
    @JsonProperty("ExpiryDate")
    private String expiryDate = "";
    
    @JsonProperty("MerchantNumber")
    private String merchantNumber = "";
    
    @JsonProperty("Signature")
    private String signature;
    
    @JsonProperty("CustomerReference")
    private String customerReference;
    
    public TokenizeRequest(CardDetails cardDetails, String merchantNumber, String merchantPassword, String acquirerId) {
        this.cardNumber = cardDetails.getCardNumber();
        this.expiryDate = cardDetails.getCardExpiryDate();
        this.merchantNumber = merchantNumber;
        this.customerReference = cardDetails.getCustomerName();
        this.signature = generateSignature(merchantNumber, merchantPassword, acquirerId);
    }
    
    private String generateSignature(String merchantId, String merchantPassword, String acquirerId) {
        if (merchantPassword == null || merchantId == null || acquirerId == null)
            throw new RuntimeException(" generate signature fail ");
        System.out.println(merchantPassword + " & " + merchantId + " & " + acquirerId);
        
        String source = String.format("%s%s%s", merchantPassword, merchantId, acquirerId);
        System.out.println(source);
        return TransactionDetails.hashSignature(source);
    }
    
    public String toXML() throws JsonProcessingException {
        XmlMapper xmlMapper = new XmlMapper();
        return xmlMapper.writeValueAsString(this).replaceAll(" xmlns=\"\"", "");
    }
}

package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BillingDetails {
    @JsonProperty("BillToAddress")
    private String billToAddress = "";
    
    @JsonProperty("BillToAddress2")
    private String billToAddress2 = "";
    
    @JsonProperty("BillToCity")
    private String billToCity = "";
    
    @JsonProperty("BillToCountry")
    private String billToCountry = "";
    
    @JsonProperty("BillToCounty")
    private String billToCounty = "";
    
    @JsonProperty("BillToEmail")
    private String billToEmail = "";
    
    @JsonProperty("BillToFirstName")
    private String billToFirstName = "";
    
    @JsonProperty("BillToLastName")
    private String billToLastName = "";
    
    @JsonProperty("BillToMobile")
    private String billToMobile = "";
    
    @JsonProperty("BillToState")
    private String billToState = "";
    
    @JsonProperty("BillToTelephone")
    private String billToTelephone = "";
    
    @JsonProperty("BillToZipPostCode")
    private String billToZipPostCode = "";
}

package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ThreeDSecureDetails {
    @JsonProperty("AuthenticationResult")
    private String authenticationResult = "";
    
    @JsonProperty("CAVV")
    private String cavv = "";
    
    @JsonProperty("ECIIndicator")
    private String eciIndicator;
    
    @JsonProperty("TransactionStain")
    private String transactionStain;
}

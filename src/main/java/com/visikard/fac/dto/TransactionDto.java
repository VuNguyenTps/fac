package com.visikard.fac.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TransactionDto {
    private double amount;
    private String orderNumber;
    private String customerReference;
    private String sessionId;
}

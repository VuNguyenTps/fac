package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class CardDetails {
    @JsonProperty("CardNumber")
    private String cardNumber = "";

    @JsonProperty("CardExpiryDate")
    private String cardExpiryDate = "";

    @JsonProperty("CardCVV2")
    private String cardCVV2 = "";

    @JsonProperty("Installments")
    private int installments = 0;

    // options

    @JsonProperty("IssueNumber")
    private String issueNumber;

    @JsonProperty("StartDate")
    private String startDate;
    
    private String customerName;
}

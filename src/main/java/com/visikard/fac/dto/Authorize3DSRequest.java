package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@JacksonXmlRootElement(localName = "Authorize3DSRequest", namespace = "http://schemas.firstatlanticcommerce.com/gateway/data")
public class Authorize3DSRequest extends FACRequest {
    
    @JsonProperty("MerchantResponseURL")
    private String merchantResponseURL;

}

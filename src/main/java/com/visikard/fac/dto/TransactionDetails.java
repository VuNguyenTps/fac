package com.visikard.fac.dto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TransactionDetails {
    @JsonProperty("AcquirerId")
    private String acquirerId;
    
    @JsonProperty("MerchantId")
    private String merchantId;
    
    @JsonProperty("OrderNumber")
    private String orderNumber = "";
    
    @JsonProperty("TransactionCode")
    private int transactionCode = 0;
    
    @JsonProperty("Amount")
    private String amount = "000000000000";
    
    @JsonProperty("Currency")
    private String currency = "840";
    
    @JsonProperty("CurrencyExponent")
    private int currencyExponent = 2;
    
    @JsonProperty("SignatureMethod")  
    private String signatureMethod = "SHA1";
    
    @JsonProperty("Signature")
    private String signature;
    
    @JsonProperty("CustomerReference")
    private String customerReference;
    
    public TransactionDetails(TransactionDto dto) {
        this.amount = TransactionDetails.formatAmount(dto.getAmount(), currencyExponent);
        this.orderNumber = dto.getOrderNumber();
        this.customerReference = dto.getCustomerReference();
    }
    
    public String generateSignature(String merchantId, String merchantPassword, String acquirerId) {
        this.merchantId = merchantId;
        this.acquirerId = acquirerId;
        if (merchantPassword == null || merchantId == null || acquirerId == null || orderNumber == null || amount == null || currency == null)
            throw new RuntimeException(" generate signature fail ");
        System.out.println(merchantPassword + " & " + merchantId + " & " + acquirerId + " & " + orderNumber + " & " + amount + " & " + currency);
        
        String source = String.format("%s%s%s%s%s%s", merchantPassword, merchantId, acquirerId, orderNumber, amount, currency);
        System.out.println(source);
        return TransactionDetails.hashSignature(source);
    }

    public static String hashSignature(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] messageDigest = md.digest(input.getBytes());

            return Base64.getEncoder().encodeToString(messageDigest);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(" hash signature fail ");
        }
    }
    
    private static String formatAmount(double amount, int currencyExponent) {
        double factor = Math.pow(10, currencyExponent);
        String temp = String.valueOf(Math.round(amount * factor));
        return StringUtils.leftPad(temp, 12, "0");
    }
}

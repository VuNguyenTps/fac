package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class FraudDetails {
    @JsonProperty("AuthResponseCode")
    private String authResponseCode;
    
    @JsonProperty("AVSResponseCode")
    private String avsResponseCode;
    
    @JsonProperty("CVVResponseCode")
    private String cvvResponseCode;
    
    @JsonProperty("SessionId")
    private String sessionId;
}

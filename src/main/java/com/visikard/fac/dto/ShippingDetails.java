package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ShippingDetails {
    @JsonProperty("ShipToAddress")
    private String shipToAddress = "";
    
    @JsonProperty("ShipToAddress2")
    private String shipToAddress2 = "";
    
    @JsonProperty("ShipToCity")
    private String shipToCity = "";
    
    @JsonProperty("ShipToCountry")
    private String shipToCountry = "";
    
    @JsonProperty("ShipToCounty")
    private String shipToCounty = "";
    
    @JsonProperty("ShipToEmail")
    private String shipToEmail = "";
    
    @JsonProperty("ShipToFirstName")
    private String shipToFirstName = "";
    
    @JsonProperty("ShipToLastName")
    private String shipToLastName = "";
    
    @JsonProperty("ShipToMobile")
    private String shipToMobile = "";
    
    @JsonProperty("ShipToState")
    private String shipToState = "";
    
    @JsonProperty("ShipToTelephone")
    private String shipToTelephone = "";
    
    @JsonProperty("ShipToZipPostCode")
    private String shipToZipPostCode = "";
}

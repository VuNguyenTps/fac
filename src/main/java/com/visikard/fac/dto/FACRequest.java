package com.visikard.fac.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class FACRequest {
    @JsonProperty("CardDetails")
    protected CardDetails cardDetails;

    @JsonProperty("TransactionDetails")
    protected TransactionDetails transactionDetails;

    @JsonProperty("FraudDetails")
    protected FraudDetails fraudDetails;
    
    public String toXML() throws JsonProcessingException {
        XmlMapper xmlMapper = new XmlMapper();
        return xmlMapper.writeValueAsString(this).replaceAll(" xmlns=\"\"", "");
    }
}

package com.visikard.fac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.visikard.fac.dto.TransactionDetails;

@SpringBootApplication(scanBasePackages = {"com.visikard.fac"})
public class FACApplication {
    
    public static void main(String[] args) {
		SpringApplication.run(FACApplication.class, args);
		
		String signatureTest = "a1B23c1234567890464748FACTEST01000000001200840";
		System.out.println("signature = " + TransactionDetails.hashSignature(signatureTest));
	}

}

<#import "/spring.ftl" as spring/>

<!DOCTYPE HTML>
<html>
   <head>
      <meta charset="UTF-8" />
      <title>Welcome</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="js/transaction.js"></script>
   </head>
   
   <body class="kaxsdc" data-event="load">
      <h1>Transaction</h1>
      <form id="transactionForm">
        <p>
            <label>Name:</label>
            <input type="text" name="sessionId" id="sessionId" value="${sessionId}">
        </p>
        <p>
            <label>Order Number:</label>
            <input type="text" name="orderNumber" value="${orderNumber}">
        </p>
        <p>
            <label>Amount:</label>
            <input type="text" name="amount" value="${amount}">
        </p>
        <input type="button" value="submit" id="submitButton">
      </form>
    
      <div id="result"></div>
      
      <script src='${KountServerUrl}/collect/sdk?m=${KountMerchantId}&s=${sessionId}'></script>
      <script>
        var client=new ka.ClientSDK(); 
        client.autoLoadEvents();
      </script>
   </body>
   
</html>
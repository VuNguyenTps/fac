$(document).ready(function() {
	$("#submitButton").on("click", function() {
		var formdata = $("#transactionForm").serializeArray();
		var data = {};
		$(formdata).each(function(index, obj) {
			data[obj.name] = obj.value;
		});

		console.log(data);
		$.ajax({
			type: 'POST',
			url: 'capture',
			data: JSON.stringify(data),
			success: function(response) { $("#result").html(response); },
			contentType: "application/json",
			dataType: 'json'
		});
	});
});